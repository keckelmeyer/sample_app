require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Eckelmeyer.com"
    assert_equal full_title("Help"), "Help | Eckelmeyer.com"
  end

end